import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UnescapePipe } from './pipes/unescape.pipe';



@NgModule({
  declarations: [
    UnescapePipe
  ],
  imports: [
    CommonModule
  ],
  exports: [UnescapePipe]
  
})
export class SharedModule { }
