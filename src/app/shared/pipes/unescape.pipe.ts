import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'unescape'
})
export class UnescapePipe implements PipeTransform {

  transform(value: string, ...args: string[]): string {
    // via https://stackoverflow.com/questions/47378033/what-the-right-way-unescape-html-entities-in-angular
    const doc = new DOMParser().parseFromString(value, 'text/html');
    return doc.documentElement.textContent;
  }

}
