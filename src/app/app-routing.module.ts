import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'quizcategory',
    loadChildren: () => import('./pages/quizcategory/quizcategory.module').then( m => m.QuizcategoryPageModule)
  },
  {
    path: 'quizcategory/:id',
    loadChildren: () => import('./pages/quizcategory/quizcategory.module').then( m => m.QuizcategoryPageModule)
  },
  {
    path: 'quizcategory/:id/:name',
    loadChildren: () => import('./pages/quizcategory/quizcategory.module').then( m => m.QuizcategoryPageModule)
  },
  {
    path: 'quizquestion/:question',
    loadChildren: () => import('./pages/quizquestion/quizquestion.module').then( m => m.QuizquestionPageModule)
  },
  {
    path: 'quizquestion',
    loadChildren: () => import('./pages/quizquestion/quizquestion.module').then( m => m.QuizquestionPageModule)
  },
  {
    path: 'correctquestion',
    loadChildren: () => import('./pages/correctquestion/correctquestion.module').then( m => m.CorrectquestionPageModule)
  },
  {
    path: 'wrongquestion',
    loadChildren: () => import('./pages/wrongquestion/wrongquestion.module').then( m => m.WrongquestionPageModule)
  },
  {
    path: 'storage',
    loadChildren: () => import('./pages/storage/storage.module').then( m => m.StoragePageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
