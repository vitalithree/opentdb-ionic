/**
 * Open source mini-game for Gamebus.
 * 
 * Based on template code from https://devdactic.com/cache-api-response-ionic/
 */

import { Component, HostListener, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { ApiService2 } from '../services/api2.service';
import { CachingService } from '../services/caching.service';
import { LoadingController } from '@ionic/angular';
import { TriviaCategory, TriviaQuestion, TriviaResultMetrics, TriviaAnswer } from '../interfaces/opentdb';
// import { Share } from '@capacitor/share'; => only on mobile devices and desktop Safari, so using https://enappd.com/blog/social-sharing-component-in-ionic-5-mobile-web-apps/168/ to render also pure HTML counterparts when needed
import { PopoverController } from '@ionic/angular';
import { SocialShareComponent } from '../components/social-share/social-share.component';

import { Platform } from '@ionic/angular'; // for hiding share button in case not supported
import { PropertyInstance, GameProperty } from '@app/interfaces/gamebus';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush 
})
export class HomePage implements OnInit {
  readonly logEnabled = true; // for central enabling/disabling of console log output
  readonly appName = "Open Trivia DB";

  /**
   * Property to control who is posting event data in the context of miniGames
   */
  readonly allowedOriginPrefixes = ["http://127.0.0.1:", "https://127.0.0.1:", "http://localhost:", "https://localhost:", "https://app3.gamebus.eu", "https://app.gamebus.eu", "https://app4b.gamebus.eu"];
  private trustParent = false;

  triviaCategories: TriviaCategory[];
  cats: TriviaCategory[];
  numTotalAnswers: Promise<TriviaResultMetrics> = this.getAggregateResultMetrics(); // see https://bitbucket.org/pietervangorp/opentdb-ionic/branch/educational/promises-vs-observables for design alternatives  

  constructor(private apiService: ApiService, private apiService2: ApiService2,
    private cachingService: CachingService,
    private loadingController: LoadingController,
    private platform: Platform,// just for educational/informative purposes (see console)
    public shareCtrl: PopoverController) {
    this.loadTriviaConfig(false);
 
  }

  async ngOnInit() {
    this.apiService.answer$.subscribe((answerMsg) => { // for each new answer given, recompute the aggregation
      this.numTotalAnswers = this.getAggregateResultMetrics();      
      this.sendToParent(answerMsg);    
    })
  }

  sendToParent(answerMsg: TriviaAnswer) {
    let frameData= this.apiService.getFrameData();
    if (this.trustParent && frameData && frameData.gameData) {
      let pInstances = frameData.gameData;
      pInstances.forEach((pi: PropertyInstance) => {
        if (pi.propertyTK.toUpperCase() === GameProperty.QUESTION_TEXT) {
          pi.value = answerMsg.question.question;
        } else if (pi.propertyTK.toUpperCase() === GameProperty.QUESTION_CORRECT) {          
          pi.value = String(answerMsg.correct);
        } else if (pi.propertyTK.toUpperCase() === GameProperty.THROUGH_HINT) {          
          pi.value = String(answerMsg.throughHint);
        } else if (pi.propertyTK.toUpperCase() === GameProperty.DIFFICULTY_LIKERT_3) {
          const answerDifficulty= answerMsg.question.difficulty.toLowerCase();
          if (answerDifficulty === "medium") {
            pi.value= "0";
          } else if (answerDifficulty === "hard") {
            pi.value= "1";
          } else { // default to easy
            pi.value= "-1";
          }
        } else if (pi.propertyTK.toUpperCase() === GameProperty.ANSWER) {
            pi.value= answerMsg.answer;
        } else {
          this.consoleLog("not sending to parent: ", pi);
        }
      })
      frameData.activityForm.propertyInstances = pInstances;      
      const message = JSON.stringify(
        frameData
      );      
      window.parent.postMessage(message, '*');
    } else {
      console.log("Not posting result to parent (due to untrusted source or due to lacking frame data)");
    }
  }

  async loadTriviaConfig(forceRefresh) {
    const loading = await this.loadingController.create({
      message: 'Loading trivia metadata...'
    });
    await loading.present();
    this.consoleLog("starting load via api2");
    this.apiService2.getCategories().then(res => {
      this.consoleLog(res);
      this.triviaCategories = res;
      this.consoleLog("done loading categories");
      loading.dismiss();
    });
  }

  /** hello world for iframe to GB Base */
  //listening to the message sent to iframe
  @HostListener('window:message', ['$event'])
  onMessage($event: MessageEvent) {
    try {
      this.consoleLog("event in onMessage in iframe from home.page.ts in trivia", $event);
      if (this.allowedOriginPrefixes.find((prefix) => {
        return $event.origin.startsWith(prefix);
      })) {
        this.consoleLog("TRUSTED", $event);
        this.trustParent = true; // store trust to also allow post back to parent
        const data = $event.data;
        if (data !== undefined && data !== null && typeof(data)!='object') {
          this.apiService.setFrameData(JSON.parse(data));
          this.consoleLog("frameData in the iframe=====", this.apiService.getFrameData());
        }
      } else {
        this.consoleLog("WARNING: not consuming parent data: extend trusted origin list in Trivia Homepage attribute.", $event.origin);
      }
    } catch (e) {
      this.consoleLog("error in onMessage", e);
      throw(e);
    };
  }

  /**
   * To fill up attribute that is awaited for
   * @returns 
   */
  async getAggregateResultMetrics(): Promise<TriviaResultMetrics> {
    const answers: TriviaQuestion[] = await this.apiService.getAllCachedAnswers();

    return this.aggregateResults(answers);
  }

  private aggregateResults(answers: TriviaQuestion[]) {
    let total: number = 0;
    let correct: number = 0;
    answers.map(a => {
      total += a.userTries;
      correct += a.userTimesCorrect;
    });
    return {
      userTimesCorrect: correct,
      userTries: total
    };
  }

  async clearCache() {
    this.cachingService.clearCachedData();
  }

  async showShareOptions(popData) {
    const shareCtrl = await this.shareCtrl.create({
      event: popData,
      component: SocialShareComponent,
      cssClass: 'backTransparent',
      backdropDismiss: true
    });
    return shareCtrl.present();
  }

  private consoleLog(msg, more?) {
    if (this.logEnabled) {
      console.log(msg, more);
    }
  }
}