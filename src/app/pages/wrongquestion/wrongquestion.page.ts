import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-wrongquestion',
  templateUrl: './wrongquestion.page.html',
  styleUrls: ['./wrongquestion.page.scss'],
})
export class WrongquestionPage implements OnInit {

  constructor(private navCtrl: NavController, public router: Router ) { 

  }

  ngOnInit() {
  }

  goBack() {
    this.navCtrl.pop();
  }

}
