import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { WrongquestionPageRoutingModule } from './wrongquestion-routing.module';

import { WrongquestionPage } from './wrongquestion.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    WrongquestionPageRoutingModule
  ],
  declarations: [WrongquestionPage]
})
export class WrongquestionPageModule {}
