import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WrongquestionPage } from './wrongquestion.page';

const routes: Routes = [
  {
    path: '',
    component: WrongquestionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WrongquestionPageRoutingModule {}
