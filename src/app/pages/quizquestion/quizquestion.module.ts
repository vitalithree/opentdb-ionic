import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { QuizquestionPageRoutingModule } from './quizquestion-routing.module';

import { QuizquestionPage } from './quizquestion.page';
import { SharedModule } from 'src/app/shared/shared.module';
import { QuestiondetailsComponent } from './questiondetails/questiondetails.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    QuizquestionPageRoutingModule,
    SharedModule
  ],
  declarations: [QuizquestionPage, QuestiondetailsComponent] // importing details component, since otherwise production build fails (solution via https://stackoverflow.com/a/66730027)
})
export class QuizquestionPageModule {}
