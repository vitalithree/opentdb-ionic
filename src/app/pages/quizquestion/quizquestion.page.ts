import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IonRadio, AlertController } from '@ionic/angular';
import { TriviaQuestion } from 'src/app/interfaces/opentdb';
import { CachingService } from 'src/app/services/caching.service';
import { PopoverController } from '@ionic/angular';
import { QuestiondetailsComponent } from './questiondetails/questiondetails.component';
import { ApiService } from '@app/services/api.service';
import { Condition, GameProperty, RuleCheck } from '@app/interfaces/gamebus';

@Component({
  selector: 'app-quizquestion',
  templateUrl: './quizquestion.page.html',
  styleUrls: ['./quizquestion.page.scss'],
})
export class QuizquestionPage implements OnInit {

  question: TriviaQuestion;
  possibleAnswers: string[];


  constructor(private activatedRoute: ActivatedRoute,
    private alertCtrl: AlertController,
    private cachingService: CachingService,
    private apiService: ApiService,
    private popoverController: PopoverController,
    private router: Router) { }

  async ngOnInit() {
    const activeQ = await this.cachingService.loadActiveQuestion();
    this.question = activeQ;
    this.possibleAnswers = this.shuffleArray(
      [this.question.correct_answer].concat(this.question.incorrect_answers));

    this.cachingService.getAnswerLog(this.question).then(
      answer => this.question = answer
    );
  }

  /** 
   * From https://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array, 
   * could be moved to a service
   */
  shuffleArray(array: any[]) {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
  }

/**
 * Function that also other mini games may want to implement
 * @returns 
 */
  public canTakeHint(): boolean {
    return this.apiService.getFrameData().negativePointsCheck.items.filter((npItem:RuleCheck)=> {
      return npItem.canFire &&
      npItem.conditions.filter((c:Condition)=> {
        return c.property.translationKey.toUpperCase() === GameProperty.THROUGH_HINT 
      }).length>0
    }).length>0;
  }

  async showConfirm(event) {
    let mcItemValue=event.detail.value;    
    const throughHint: boolean= mcItemValue?false:true;
    const prompt = await this.alertCtrl.create({
      header: 'Submit?',
      message: throughHint?"Are you sure you want that hint?":mcItemValue,
      buttons: [
        {
          text: 'OK',
          role: 'OK',
          handler: () => {
            this.question.userTries++;            
            if (throughHint){
              mcItemValue= this.question.correct_answer; // take the hint
            }
            const isCorrect: boolean= (mcItemValue == this.question.correct_answer);
            if (isCorrect) {
              this.question.userTimesCorrect++;
              this.router.navigate(['/correctquestion']);
            } else {
              this.router.navigate(['/wrongquestion']);
            }
            this.cachingService.storeAnswerLog(this.question).then(() =>
              this.apiService.broadcastAnswer({
                question: this.question,
                correct: isCorrect,
                answer: mcItemValue,
                throughHint: throughHint
              })
            )
          }
        },
        {
          text: 'Cancel',
          role: 'Cancel',
          handler: () => {
            // do nothing
          }
        }
      ]
    });
    await prompt.present();
    /** alternative approach to results checking
    const result= await prompt.onDidDismiss();
    console.log(result);
    if (result.role.toLowerCase()=="ok") {

    }
     */
  }

  async detailsPopover(ev: any) {
    const popover = await this.popoverController.create({
      component: QuestiondetailsComponent,
      event: ev,
      cssClass: 'popover_setting',
      componentProps: {
        question: this.question
      },
      translucent: true
    });

    popover.onDidDismiss().then((result) => {
      console.log(result.data);
    });

    return await popover.present();
    /** Sync event from popover component */

  }

}

