import { Component, Input, OnInit } from '@angular/core';
import { PopoverController, } from '@ionic/angular';

@Component({
  selector: 'app-questiondetails',
  templateUrl: './questiondetails.component.html',
  styleUrls: ['./questiondetails.component.scss'],
})
export class QuestiondetailsComponent implements OnInit {

  @Input("question") question;
  constructor(private popoverController: PopoverController) { 
  
  }

  ngOnInit() {
    console.log("in pop-over question details");
  }

  dismiss() {
    this.popoverController.dismiss();
  }
}
