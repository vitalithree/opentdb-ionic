import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { QuizquestionPage } from './quizquestion.page';

const routes: Routes = [
  {
    path: '',
    component: QuizquestionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class QuizquestionPageRoutingModule {}
