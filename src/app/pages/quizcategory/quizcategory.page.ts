import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../../services/api.service';
import { CachingService } from 'src/app/services/caching.service';
import { LoadingController } from '@ionic/angular';
import { TriviaQuestionsFromAPI, TriviaQuestion } from 'src/app/interfaces/opentdb';
import { finalize } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Component({
  selector: 'app-quizcategory',
  templateUrl: './quizcategory.page.html',
  styleUrls: ['./quizcategory.page.scss'],
})
export class QuizcategoryPage implements OnInit {
  id = 0;
  name: string = "(no name)";
  questions: TriviaQuestion[];

  constructor(private activatedRoute: ActivatedRoute, private apiService: ApiService,
    private cachingService: CachingService,
    private loadingController: LoadingController,
    private router: Router,
  ) {
    this.loadTriviaQuestions(false);
  }

  ngOnInit() {
    this.id = parseInt(this.activatedRoute.snapshot.paramMap.get('id'));
    this.name = this.activatedRoute.snapshot.paramMap.get('name');
  }

  async loadTriviaQuestions(forceRefresh) {
    const loading = await this.loadingController.create({
      message: 'Loading questions...'
    });
    await loading.present();

    this.apiService.getTriviaQuestions(this.id, forceRefresh).subscribe(res => {        
      this.questions = res;      
    },((e)=>{console.error(e);throwError(e);}),
    ()=>{loading.dismiss();});    
    loading.dismiss(); // unfortunately, I had to add this redundantly, since the previous dismiss is not executed for some funky reason
  }

  async goToQuestion(question: TriviaQuestion) {
    await this.cachingService.storeActiveQuestion(question);
    this.router.navigate(['/quizquestion']);
  }

}
