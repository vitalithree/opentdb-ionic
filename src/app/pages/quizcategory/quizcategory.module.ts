import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { QuizcategoryPageRoutingModule } from './quizcategory-routing.module';

import { QuizcategoryPage } from './quizcategory.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    QuizcategoryPageRoutingModule,
    SharedModule    
  ],
  declarations: [QuizcategoryPage],
  providers: []
})
export class QuizcategoryPageModule {}
