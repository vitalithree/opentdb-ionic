import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { QuizcategoryPage } from './quizcategory.page';

const routes: Routes = [
  {
    path: '',
    component: QuizcategoryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class QuizcategoryPageRoutingModule {}
