import { Component, OnInit } from '@angular/core';
import { CachingService } from '../../services/caching.service';
import { StorageItem } from '../../interfaces/StorageItem';

@Component({
  selector: 'app-storage',
  templateUrl: './storage.page.html',
  styleUrls: ['./storage.page.scss'],
})
export class StoragePage implements OnInit {
  storageItems: StorageItem[];
  constructor(private cachingService: CachingService) {

    this.storageItems = [];

  }

  ngOnInit() {
    let i: number= 0;
    this.cachingService.getStorage().forEach((v,k) => {
      this.storageItems.push({key:k,value:JSON.stringify(v),index:i++});
    });
  }

}
