import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TriviaAnswer, TriviaQuestion } from '@app/interfaces/opentdb';
import { CachingService } from '@app/services/caching.service';

@Component({
  selector: 'app-correctquestion',
  templateUrl: './correctquestion.page.html',
  styleUrls: ['./correctquestion.page.scss'],
})
export class CorrectquestionPage implements OnInit {

  public question: Promise<TriviaQuestion>= this.cachingService.loadActiveQuestion();

  constructor(private router: Router,
    private cachingService: CachingService) { }

  ngOnInit() {
  }

  public toHome() {
    this.router.navigate(['/home']).then(() => {
    });
  }
}
