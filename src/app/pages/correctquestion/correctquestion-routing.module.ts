import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CorrectquestionPage } from './correctquestion.page';

const routes: Routes = [
  {
    path: '',
    component: CorrectquestionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CorrectquestionPageRoutingModule {}
