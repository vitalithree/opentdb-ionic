import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CorrectquestionPageRoutingModule } from './correctquestion-routing.module';

import { CorrectquestionPage } from './correctquestion.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CorrectquestionPageRoutingModule
  ],
  declarations: [CorrectquestionPage]
})
export class CorrectquestionPageModule {}
