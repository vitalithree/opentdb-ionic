/**
 * Generated with http://json2ts.com/ from https://opentdb.com/api_category.php
 */

export interface TriviaCategory {
    id: number;
    name: string;
    numQuestions: number;// PVG added to join in data from CategoryQuestionCount, initially '?', so not number
}

/**
 * Hand-crafted extension by PVG
 */
export interface TriviaResultMetrics {
    userTries: number; // PVG for counting how many times the user tried
    userTimesCorrect: number; // PVG for counting how many times correct (should be <= 1 in v1)
}

/**
 * Generated with http://json2ts.com/ from https://opentdb.com/api_count.php?category=9 (9 is arbitrary)
 */

export interface CategoryQuestionCount {
    total_question_count: number;
    total_easy_question_count: number;
    total_medium_question_count: number;
    total_hard_question_count: number;
}

export interface CategoryQuestionCountResult {
    category_id: number;
    category_question_count: CategoryQuestionCount;
}

export interface TriviaQuestion extends TriviaResultMetrics { // PVG added extension to merge in results performance
    category: string;
    type: string;
    difficulty: string;
    question: string;
    correct_answer: string;
    incorrect_answers: string[];
}

export interface TriviaQuestionsFromAPI {
    response_code: number;
    results: TriviaQuestion[];
}

export interface TriviaAnswer {
    question: TriviaQuestion;
    answer: string;
    correct: boolean;
    throughHint: boolean;    
}
