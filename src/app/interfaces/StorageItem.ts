export interface StorageItem {
 key,
 value: string,
 index: number
}