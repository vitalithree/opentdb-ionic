import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { CategoryQuestionCount, TriviaCategory } from '../interfaces/opentdb';
import { Observable, throwError } from 'rxjs';
import { retry, catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService2 {

  categories: TriviaCategory[];

  // API path
  base_path = 'https://opentdb.com/';

  constructor(private http: HttpClient) { }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  // Handle API errors
  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  };
/*
  public async getCategories(): Promise<TriviaCategory[]> {
    if (this.categories) {
      return this.categories;
    } else {
      let promise = new Promise<TriviaCategory[]>((resolve, reject) => {
        this.getCategories_O().toPromise().then(
          cats => {
            let cats2: TriviaCategory[] = cats.map(cat => {
              this.getCountOfCategory_O(cat.id).toPromise().then(
                countRes => {
                  cat.numQuestions = countRes.total_question_count;
                }
              );
              return cat;
            });
            this.categories = cats2
            resolve(cats2);
          },
          error => {
            console.error(error);
          });
      });
      return promise;
    }
  }
  */

  public async getCategories(): Promise<TriviaCategory[]> {
    let promise = new Promise<TriviaCategory[]>((resolve, reject) => {
      if (!this.categories) {        
        this.getCategories_O().toPromise().then(
          cats => {
            let waitForPromises:Promise<any>[]= [];
            let cats2: TriviaCategory[] = cats.map(cat => {
              let lastPromise= this.getCountOfCategory_O(cat.id).toPromise();
              waitForPromises.push(lastPromise);
              lastPromise.then(
                countRes => {
                  cat.numQuestions = countRes.total_question_count;                  
                }
              );
              return cat;
            });
            this.categories = cats2;   
            Promise.all(waitForPromises).then((promiseResults) => {
              resolve(cats2);
            });
            
          },
          error => {
            console.error(error);
            reject(error);
          });
      } else {
        resolve(this.categories);
      }
    });
    return promise;

  }
  
  private getCategories_O(): Observable<TriviaCategory[]> {
    return this.http
      .get<TriviaCategory[]>(this.base_path + 'api_category.php')
      .pipe(
        map(res => res['trivia_categories']),
        retry(2),
        catchError(this.handleError)
      )
  }

  private getCountOfCategory_O(id: number): Observable<CategoryQuestionCount> {
    return this.http
      .get<TriviaCategory[]>(this.base_path + 'api_count.php?category=' + id.toString())
      .pipe(
        map(res => res['category_question_count']),
        retry(2),
        catchError(this.handleError)
      )
  }

}