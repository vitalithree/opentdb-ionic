/**
 * Capacitor v3 version of https://devdactic.com/cache-api-response-ionic/
 */

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { from, Observable, of, Subject, throwError } from 'rxjs';
import { map, switchMap, delay, tap, finalize, catchError } from 'rxjs/operators';
import { CachingService } from './caching.service';
import { Network } from '@capacitor/network';
import { ToastController } from '@ionic/angular';
import { User } from '../interfaces/randomusers';
import { CategoryQuestionCountResult, TriviaQuestion, CategoryQuestionCount, TriviaAnswer } from '../interfaces/opentdb';
import { TriviaCategory } from '../interfaces/opentdb';
import { ApiService2 } from './api2.service';
import { FrameData } from '@app/interfaces/gamebus';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private answerSubject = new Subject<TriviaAnswer>();// a stream of answers, such that on multiple pages you can display answers and call aggregations
  public readonly answer$ = this.answerSubject.asObservable();

  connected = true;
  readonly triviaQuestionPerCategory: number = 100;
  private triviaCategories: TriviaCategory[];  
  private frameData: FrameData;

  constructor(private http: HttpClient, private cachingService: CachingService, private toastController: ToastController, private apiService2: ApiService2) {
    Network.addListener('networkStatusChange', async status => {
      this.connected = status.connected;
    });

    // Can be removed once #17450 is resolved: https://github.com/ionic-team/ionic/issues/17450
    this.toastController.create({ animated: false }).then(t => { t.present(); t.dismiss(); });
  }

  // App-specific API Functions
  setFrameData(fd: FrameData) {
    this.frameData= fd;
  }

  getFrameData(): FrameData {
    if (this.frameData) {
      return this.frameData;
    } else {
      console.log("WARNING: using mocked FrameData for use in stand-alone mode");
      return { // mock object for use outside a minigame setting
        activityForm: null,
        gameData: [],
        scheme: null,
        lastActivity: null,
        negativePointsCheck: {
          canFireAll: false,
          items: []
        }
      }
    }
  }
  
  /**
  * Instruct listeners to answer$ to react to changes
  */
  public broadcastAnswer(answer: TriviaAnswer) {
    this.answerSubject.next(answer);
  }

  async getAllCachedAnswers(): Promise<TriviaQuestion[]> {
    return this.cachingService.getAllCachedAnswers();
  }

  observeCachedAnswers(): Observable<TriviaQuestion[]> {
    return from(this.cachingService.getAllCachedAnswers());
  }

  /**
   * @deprecated
   * @param triviaCategories 
   * @param forceRefresh 
   * @returns 
   */
  getTriviaCategoryMetadata(triviaCategories: TriviaCategory[], forceRefresh: boolean): Observable<CategoryQuestionCountResult[]> {
    let res: CategoryQuestionCountResult[] = [];
    for (var cat of triviaCategories) {
      const url = 'https://opentdb.com/api_count.php?category=' + cat.id;
      this.getData(url, forceRefresh).subscribe(
        subRes => {
          res.push(subRes);
        }
      );
    }
    return of(res);
  }

  getTriviaQuestions(category: number, forceRefresh: boolean): Observable<TriviaQuestion[]> {
    let questionsSubject = new Subject<TriviaQuestion[]>();
    let questions$ = questionsSubject.asObservable();

    let result: TriviaQuestion[] = [];
    const categories = this.apiService2.getCategories();
    categories.then(cats => {
      const details = cats.find(cat => cat.id === category);
      const url = 'https://opentdb.com/api.php?amount=' + Math.min(this.triviaQuestionPerCategory, details ? details.numQuestions : 0) +
        '&category=' + category; // &difficulty=medium&type=multiple';
      this.getData(url, forceRefresh).pipe(
        map(res => res['results'])
      ).subscribe(subRes => {
        result= subRes;
        questionsSubject.next(result)
      });
    },
      error => {
        console.error(error);
        throwError(error);
      });
      questionsSubject.next(result);
    return questions$;    
  }

  getNumQuestionsForCategory(cat: TriviaCategory, forceRefresh: boolean): Observable<CategoryQuestionCount> {
    const url = 'https://opentdb.com/api_count.php?category=' + cat.id;
    return this.getData(url, forceRefresh).pipe(
      map(res => res['category_question_count'])
    );
  }

  getUsers(forceRefresh: boolean): Observable<User[]> {
    const url = 'https://randomuser.me/api?results=10';
    return this.getData(url, forceRefresh).pipe(
      map(res => res['results'])
    );
  }

  // Reusable Caching Functions

  private getData(url, forceRefresh = false): Observable<any> {
    if (!this.connected) { // Handle offline case
      this.toastController.create({
        message: 'You are viewing offline data.',
        duration: 2000
      }).then(toast => {
        toast.present();
      });
      return from(this.cachingService.getCachedRequest(url));
    } else { // Handle connected case
      if (forceRefresh) {
        // Make a new API call
        return this.callAndCache(url);
      } else {
        // Check if we have cached data
        const storedValue = from(this.cachingService.getCachedRequest(url));
        return storedValue.pipe(
          switchMap(result => {
            if (!result) {
              // Perform a new request since we have no data
              return this.callAndCache(url);
            } else {
              // Return cached data
              return of(result);
            }
          })
        );
      }
    }
  }

  private callAndCache(url): Observable<any> {
    return this.http.get(url).pipe(
      //delay(2000), // Only for testing!
      tap(res => {
        // Store our new data
        this.cachingService.cacheRequest(url, res);
      })
    )
  }
}
