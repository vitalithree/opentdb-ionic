import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import * as CordovaSQLiteDriver from 'localforage-cordovasqlitedriver'
import { from, Observable, of } from 'rxjs';
import { TriviaQuestion, TriviaQuestionsFromAPI } from '../interfaces/opentdb';

/** 
 * A simple caching service
 * 
 * Based on: https://devdactic.com/cache-api-response-ionic/
 */

// Expire time in seconds 
const TTL = 60 * 60;

// Key to identify only cached API data
const CACHE_KEY_PREFIX = '_mycached_';
const ANSWER_LOG_KEY_PREFIX = CACHE_KEY_PREFIX + "RES_";
const ACTIVE_QUESTION_KEY = CACHE_KEY_PREFIX + "ACTIVE_QUESTION";

@Injectable({
  providedIn: 'root'
})
export class CachingService {

  constructor(private storage: Storage) { }

  /** 
   * Setup Ionic Storage
   */
  async initStorage() {
    await this.storage.defineDriver(CordovaSQLiteDriver);
    await this.storage.create();
    console.log("storage initialized");
  }

  /**
   * For debugging purposes
   * @returns the storage within this cache
   */
  public getStorage(): Storage {
    return this.storage;
  }

  /** 
   * Store active question as complex object to avoid having to expose details in the HTTP query string
  */
  async storeActiveQuestion(question: TriviaQuestion) {
    this.storage.set(ACTIVE_QUESTION_KEY, question);
  }

  /** 
   * Load back (see storeActiveQuestion)
  */
  async loadActiveQuestion(): Promise<TriviaQuestion> {
    const storedValue: TriviaQuestion = await this.storage.get(ACTIVE_QUESTION_KEY);
    return storedValue;
  }


  /** 
   * Answer storage local, for supporting aggregations on #correct, etc.
   * Note that the 'apiService.broadcastAnswer' method handles API based persistence to GameBus (via onMessage/postMessage)
  */
  async storeAnswerLog(answer: TriviaQuestion) {
    const storedValue: TriviaQuestion = await this.storage.get(this.storageKeyFromAnswer(answer));
    if (!storedValue) {
      this.storage.set(this.storageKeyFromAnswer(answer), answer);
    } else {
      storedValue.userTries = answer.userTries; // we only overwrite the data that does not come from the trivia API
      storedValue.userTimesCorrect = answer.userTimesCorrect;
      this.storage.set(this.storageKeyFromAnswer(answer), storedValue);
    }
  }

  /**
   * Retrieve back answer (@see storeAnswerLog)
   * @param answer 
   * @returns 
   */
  async getAnswerLog(answer: TriviaQuestion): Promise<TriviaQuestion> {
    const storedValue: TriviaQuestion = await this.storage.get(this.storageKeyFromAnswer(answer));
    if (!storedValue) {
      answer.userTries = 0;
      answer.userTimesCorrect = 0;
      return answer;
    } else {
      return storedValue;
    }
  }


  async getAllCachedAnswers(): Promise<TriviaQuestion[]> {
    let res: TriviaQuestion[] = [];
    const st= await this.storage;
    await st.forEach((v, k) => {
      if (k.startsWith(ANSWER_LOG_KEY_PREFIX)) {
        res.push(v);
      } 
    });
    return res;
  }
  
  /**
   * Store request data
   */
  cacheRequest(url: string, data): Promise<any> {
    const validUntil = (new Date().getTime()) + TTL * 1000;
    return this.storage.set(this.storageKeyFromUrl(url), { validUntil, data });
  }

  /** 
   * Try to load cached data
   */
  async getCachedRequest(url: string): Promise<any> {
    const currentTime = new Date().getTime();

    const storedValue = await this.storage.get(this.storageKeyFromUrl(url));

    if (!storedValue) {
      return null;
    } else if (storedValue.validUntil < currentTime) {
      await this.storage.remove(url);
      return null;
    } else {
      return storedValue.data;
    }
  }

  /** 
   * Helper to avoid mistakes in concatenation 
   */
  private storageKeyFromUrl(url: string) {
    return `${CACHE_KEY_PREFIX}${url}`;
  }
  /** 
   * Helper to avoid mistakes in concatenation 
   */
  private storageKeyFromAnswer(answer: TriviaQuestion) {
    return ANSWER_LOG_KEY_PREFIX + answer.category + "_" + answer.question;
  }

  /**
   * Remove all cached data & files
   */
  async clearCachedData() {
    const keys = await this.storage.keys();

    keys.map(async key => {
      if (key.startsWith(CACHE_KEY_PREFIX)) {
        await this.storage.remove(key);
      }
    });
  }

  /**
   * debug output
   */
  async dumpCachedData() {
    const keys = await this.storage.keys();

    keys.map(async key => {
      const val = await this.storage.get(key);
      console.log(val);
    });
  }

  /** 
   * Example to remove one cached URL
   */
  async invalidateCacheEntry(url: string) {
    await this.storage.remove(this.storageKeyFromUrl(url));
  }
}
