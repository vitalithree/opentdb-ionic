import { TestBed } from '@angular/core/testing';

import { GamebusService } from './gamebus.service';

describe('GamebusService', () => {
  let service: GamebusService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GamebusService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
