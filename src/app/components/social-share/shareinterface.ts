export interface ShareInterface {
    title: string,
    logo: string,
    href: string
}
