/**
 * Based very loosely on https://enappd.com/blog/social-sharing-component-in-ionic-5-mobile-web-apps/168/,
removed all capacitor parts, since they would not work well in my tests 
* 
* So, just using cross platform HTML capabilities
*/

import { Component, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment'; // was fighting with @env, so hacked away relative path
import { PopoverController, Platform } from '@ionic/angular';
import { ShareInterface } from './shareInterface';

@Component({
  selector: 'app-social-share',
  templateUrl: './social-share.component.html',
  styleUrls: ['./social-share.component.scss'],
})
export class SocialShareComponent implements OnInit {

  public sharingList = environment.socialShareOption;
  loader: any = null;
  sharingText = 'Look at my score in Open Triva!';
  emailSubject = this.sharingText;
  recipient = [''];
  sharingImage = ['https://opentdb.com/images/logo.png'];
  sharingUrl = 'https://store.enappd.com';
  constructor(
    private ctrl: PopoverController,
    private plt: Platform
  ) { }

  ngOnInit() { }
  closeCtrl() {
    this.ctrl.dismiss();
  }

  /**
   * Not used by PVG, keeping here just for historical reference
   * @param shareData 
   */
  async shareVia(shareData) {
    if (this.plt.is('cordova')) {
      console.log("with cordova");
      // not needed for cross-platform variant, just using links, no calls to this shareVia function
    } else {
      const shareClick: ShareInterface= shareData;
      console.log("desktop, no cordova");
      console.log(shareClick);
    }
  }
  
}