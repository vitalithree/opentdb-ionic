export const environment = {
  production: true,
  socialShareOption: [
    {
      title: 'Facebook',
      logo: 'assets/socialShare/facebook.png',
      href: 'https://facebook.com/sharer/sharer.php?display=page&u=https://minigames.gamebus.eu/opentrivia/&quote=My+latest+OpenTrivia+score...'
    },
    {
      title: 'Twitter',
      logo: 'assets/socialShare/twitter.png',
      href: 'https://twitter.com/intent/tweet/?hashtags=opentrivia&text=My+latest+OpenTrivia+score+at+https://minigames.gamebus.eu/opentrivia/'
    }  ]
};
