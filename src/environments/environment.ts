// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  socialShareOption: [
    {
      title: 'Facebook',
      logo: 'assets/socialShare/facebook.png',
      href: 'https://facebook.com/sharer/sharer.php?display=page&u=https://minigames.gamebus.eu/opentrivia/&quote=My+latest+OpenTrivia+score...'
    },
    {
      title: 'Twitter',
      logo: 'assets/socialShare/twitter.png',
      href: 'https://twitter.com/intent/tweet/?hashtags=opentrivia&text=My+latest+OpenTrivia+score+at+https://minigames.gamebus.eu/opentrivia/'
    },/*
    {
      title: 'Pinterest',
      logo: 'assets/socialShare/pinterest.png',
      href: 'https://pinterest.com/pin/create/link/?url=https://minigames.gamebus.eu/opentrivia/&media=https://opentdb.com/images/logo.png&description=My+latest+OpenTrivia+score...'
    },
    {
      title: 'Email',
      logo: 'assets/socialShare/mail.png',
      href: 'mailto:?subject=My latest OpenTrivia session...&body=I played OpenTrivia. Come join me at https://minigames.gamebus.eu/opentrivia/'
    }*/
  ]
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
