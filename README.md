## What

A socially gamified Triva app, based  [Ionic](https://ionicframework.com/), [Open Trivia DB](https://opentdb.com/), and [GameBus](https://www.gamebus.eu/).

## Who

* **By Who?**  The main author is [Pieter Van Gorp](https://www.pietervangorp.com). When reusing code, attribution is made via comments in the sources.
* **For Whom?**

  * The project aims to inspire and assist game developers to integrate new or existing games into the GameBus framework.
  * There are also some braches with "educational" detours, experimenting with alternative designs in Typescript/Angular. These branches were created since they had an educational value to the developer, and may also help others in better understanding Typescript/Angular/Ionic.

## User Experience

As an end-user, you can play Trivia (i.e., answer quiz questions) as a mini-game inside of a bigger "life game". That life game can be defined with your own rules, or those rules can be set by someone else. In any case, you decide whether or not you join one or more of such life games.

### Example Use Case

Imagine you want to become more physically active in order to adhere to lose weight, or just to feel better. Still, you fail to realize your intended behavior change. Imagine further that you are keen learning new things by playing quizes. Then, you would benefit from a life game, where:

1. Being physically active yields points (e.g., 10 point every time you get out of your office chair and make at least 50 steps (as tracked by your smartphone or smartwatch).
2. Answering a Trivia quiz question correctly costs you just one point, but answering one incorrectly costs two points. Also, taking a "hint" (i.e., just asking what is the correct answer) costs you five points.

You could put yourself up to the challenge of collecting 100 points in one week. If you manage to reach that goal, you reward yourself with a [fine beer](https://tripelkarmeliet.com/en). If you would fail, you would refrain from indulging in that pleasure. With such game rules, you would probably score some initial points via correct Trivia answers, but the idea would be to get you stuck at some point. In order to make progress, you would need to "buy" a hint, and for that you would have to get up and go for a healthy walk.

### Other Use Cases

Perhaps physical activity is totally not your relevant to you. However, you are very socially engaged. You like to play Trivia games, and you believe you are good at it too, while your best friend hates Trivia games but excells at playing old school Tetris. Via the framework demonstrated here, someone could also make Tetris a GameBus mini-game and then you could play Trivia games to compete with the Tetris sessions played by your friend. You could track progress via GameBus and strive just towards friend-fame, or make a bet on something every few weeks (e.g., who pays for a good cup of coffee). Probably, you would like to figure out over time what is the points distribution that keeps you both maximally engaged. According to [flow theory](https://en.wikipedia.org/wiki/Flow_(psychology)), the points distribution would have to be for you both challenging enough to not bore you but also feasible enough at the same time. If someone would decide on your behalf how Tetris points relate to Trivia points, chances are high that either your friend or you would not remain [in flow](https://en.wikipedia.org/wiki/Flow_(psychology)https:/). This is exactly why we champion the idea of making Trivia and Tetris so-called "mini-games" in a larger game, where the points are easily adjustable.

At the same time, many people don't have the skills or time to define custom game rules. Therefore, it would be valuable if someone else would setup a nice library of pre-defined challenges to pick from. This presents an opportunity for stakeholders that have an incentive to keep others healthy (that could be insurance companies, employers, etc.) but do not wish build maintain their own game infrastructure. 

### Theoretical Generalization

In 2017, we have generalized such use casess in the so-called concept of [Unified Health Gamification](https://ieeexplore.ieee.org/document/8037858https:/) (UHG). A free (author) copy of that article can be downloaded from the [TU/e libary pages](https://research.tue.nl/nl/publications/unified-health-gamification-can-significantly-improve-well-being-https:/). Since 2017, we have evaluated various variations of the UHG concepts in various settings (e.g., in schools and among employees).  Contact us via the contact page on [the GOAL website](https://healthgoal.eu/) if you are eager to learn more about those scientific experiments.

## Architecture

Anyhow, these pages are to demonstrate how to make your own mini-games, so let's get a bit more technical. All experiments were performed using the [GameBus](https://www.gamebus.eu/) platform. Until 2021, it was only possible to integrate new "games" (or other data sources) by using a Web API (OAuth/REST based). In the following, we describe the novel simpler alternative that we demonstrate through the GameBus Trivia mini-game:

* The Triva app from this repository can be run as a (progressive) web application, on its own web server, so you are not dependent on the GameBus platform providers to deploy (y)our updates/tweaks every time you we/you have any. It was originally developed as a stand-alone Trivia app, and can still function stand-alone (albeit then without the UHG features).
* The GameBus platform has its own web application for managing friends circles, leaderboards, chat messages, and render activity feeds. The platform also provides off-the-shelf integrations to yet other personal data platforms like [Google Fit](https://www.google.com/fit/), [Fitbit](https://www.fitbit.com/), [Strava](https://www.strava.com/), etc. The GameBus source code has nothing that is coupled to a specific mini-game. Instead, it enables to store the name, location (and other meta-data) of mini-games. That meta-data can be entered through a self-service portal for game developers, but we also provide support to such developers and encourage them to reach out and discuss which meta-data would enable the best overall user experience.
* The communication between the Trivia app and GameBus does not require any web API programmming. Instead, all communication goes via one simple JavaScript interface, for which we give some example code snippets below.

![Architecture](https://bitbucket.org/vitalithree/opentdb-ionic/raw/01b7c6a6456d218b68a06e3128e182a2520f096d/documentation/TriviaMiniGame.jpg)

## Key Code Snippets
The following snippet from [GameBus.ts](https://bitbucket.org/vitalithree/opentdb-ionic/src/master/src/app/interfaces/gamebus.ts) clarifies what is the typed interface between the GameBus app and its embedded mini-games:

```typescript
/**
 * Trivia Specific Part
 */
 export enum GameProperty {
    QUESTION_TEXT= "QUESTION_TEXT",
    ANSWER= "ANSWER",
    DIFFICULTY_LIKERT_3= "DIFFICULTY_LIKERT_3",
    THROUGH_HINT= "THROUGH_HINT",
    QUESTION_CORRECT= "QUESTION_CORRECT"
  } 

/**
 * General Part
 */

export interface FrameData {
    gameData: PropertyInstance[];
    // other parts of the interface are not critical for a basic integration
}
```

The enumeration `GameProperty` defines which are the properties a Trivia game session is producing: this involves the question, the answer, the difficulaty level (on a scale of three from easy to hard), whether a hint was consumed, and whether the answer was correct. Based on those properties, the UHG rules can be defined on the GameBus side.

The interface `FrameData` prescribs that the payload of a message between GameBus and Trivia (in either direction) will always carry a list of Property Instances. Those are further typed as:

```typescript
export interface PropertyInstance {
    value: string;
    propertyTK: string; 
}
```

The `value` part of `PropertyInstance` will carry actual Trivia question-specific payload and the `PropertyInstance` will give meaning to the payload (e.g., it will state that the payload carries `GameProperty.THROUGH_HINT` data)

A valid instance of `FrameData.gameData` for Trivia would be:

```json
[
    {
        "value": "false",
        "propertyTK": "THROUGH_HINT"
    },
    {        
        "value": "What is the name of the default theme that is installed with Windows XP?",
        "propertyTK": "QUESTION_TEXT"
    },
    {        
        "value": "Neptune",
        "propertyTK": "ANSWER"
    },
    {    
        "value": "0",
        "propertyTK": "DIFFICULTY_LIKERT_3"
    },
    {        
        "value": "false",
        "propertyTK": "QUESTION_CORRECT"
    }
]
```

So, the Trivia MiniGame would tell to GameBus that the player has just answered the question "What is the name of the default theme that is installed with Windows XP?" with the answer "Neptune" and that answer was *not* correct (`QUESTION_CORRECT==false`). Following the example from one of the use cases above, there would be a game rule that would then take one point from the player's score in GameBus.

By the way, the correct answer to the above Trivia question was Luna 🤓.

Back to actual code of the Trivia mini-game: from [home.page.ts](https://bitbucket.org/vitalithree/opentdb-ionic/src/master/src/app/home/home.page.ts), which is the app's homepage:

```typescript
async ngOnInit() {
    this.apiService.answer$.subscribe((answerMsg) => { 
      this.numTotalAnswers = this.getAggregateResultMetrics();      
      this.sendToParent(answerMsg);    
    })
  }

  sendToParent(answerMsg: TriviaAnswer) {
    let frameData= this.apiService.getFrameData();
    if (this.trustParent && frameData && frameData.gameData) {
      let pInstances = frameData.gameData;
      pInstances.forEach((pi: PropertyInstance) => {
        if (pi.propertyTK.toUpperCase() === GameProperty.QUESTION_TEXT) {
          pi.value = answerMsg.question.question;
        } else if (pi.propertyTK.toUpperCase() === GameProperty.QUESTION_CORRECT) {          
          pi.value = String(answerMsg.correct);
        } else if (pi.propertyTK.toUpperCase() === GameProperty.THROUGH_HINT) {          
          pi.value = String(answerMsg.throughHint);
        } else if (pi.propertyTK.toUpperCase() === GameProperty.DIFFICULTY_LIKERT_3) {
          const answerDifficulty= answerMsg.question.difficulty.toLowerCase();
          if (answerDifficulty === "medium") {
            pi.value= "0";
          } else if (answerDifficulty === "hard") {
            pi.value= "1";
          } else { // default to easy
            pi.value= "-1";
          }
        } else if (pi.propertyTK.toUpperCase() === GameProperty.ANSWER) {
            pi.value= answerMsg.answer;
        } else {
          this.consoleLog("not sending to parent: ", pi);
        }
      })
      frameData.activityForm.propertyInstances = pInstances;      
      const message = JSON.stringify(
        frameData
      );      
      window.parent.postMessage(message, '*');
    } else {
      console.log("Not posting result to parent (due to untrusted source or due to lacking frame data)");
    }
  }
```

This code first installs a listener for changes to the app's data related to Trivia answers. Then, every time such an answer is generated, it posts a message to the parent (i.e., to GameBus). The check `this.trustParent` is perhaps overkill, but ensures that no other website can embed the mini-game for unforeseen and potentially malicious uses. Then, the line `pInstances.forEach((pi: PropertyInstance) => {` ensures we visit all the metadata that we declared also in the `GameProperty` enum. The if-statement then maps from the mini-game internal data structure to the structure of the interface with GameBus.

The [home.page.ts](https://bitbucket.org/vitalithree/opentdb-ionic/src/master/src/app/home/home.page.ts) component also ensures that the mini-game correctly consumes data that is sent in the other direction (i.e., from GameBus to the mini-game):

```typescript
@HostListener('window:message', ['$event'])
  onMessage($event: MessageEvent) {
    try {
      this.consoleLog("event in onMessage in iframe from home.page.ts in trivia", $event);
      if (this.allowedOriginPrefixes.find((prefix) => {
        return $event.origin.startsWith(prefix);
      })) {
        this.consoleLog("TRUSTED", $event);
        this.trustParent = true; // store trust to also allow post back to parent
        const data = $event.data;
        if (data !== undefined && data !== null) {
          this.apiService.setFrameData(JSON.parse(data));
          this.consoleLog("frameData in the iframe=====", this.apiService.getFrameData());
        }
      } else {
        this.consoleLog("WARNING: not consuming parent data: extend trusted origin list in Trivia Homepage attribute.");
      }
    } catch (e) {
      this.consoleLog("error in onMessage", e);
      throw(e);
    };
  }
```

This function will be called on the iframe in which the mini-game is embedded. Again, the check `if (this.allowedOriginPrefixes.find((prefix) => {` relates to arguably over-designed security (but as it is so simply we demonstrate it anyhow). Then, the line `this.apiService.setFrameData(JSON.parse(data));` stores the relevant payload for further processing in the mini-game. In particular, the payload already includes an array of `PropertyInstance` in `FrameData.gameData`, such that indeed the `sendToParent` function that we discussed before can simply loop over it and check for the relevant property names. 

The final code snippet comes from [quizquestion.page.ts](https://bitbucket.org/vitalithree/opentdb-ionic/src/master/src/app/pages/quizquestion/quizquestion.page.ts) and demonstrates how the mini-game can check whether a certain hint is allowed to be taken:

```typescript
/**
 * Function that also other mini games may want to implement
 * @returns 
 */
  public canTakeHint(): boolean {
    return this.apiService.getFrameData().negativePointsCheck.items.filter((npItem:RuleCheck)=> {
      return npItem.canFire &&
      npItem.conditions.filter((c:Condition)=> {
        return c.property.translationKey.toUpperCase() === GameProperty.THROUGH_HINT 
      }).length>0
    }).length>0;
  }
```

Please recall that we aim for an extremely loose coupling between GameBus and its mini-games. So, mini-games are not supposed to check leaderboard scores, etc. GameBus does that centrally, and mini-game developers should not bother. Instead, they should check via the `FrameData` that was received from GameBus whether the `negativePointsCheck` array contains a rule item which represents a hint, and then whether that rule is allowed to fire. While this example code is arguably not straight-forward, it does cover a very wide range of scenario's (including cases where at the GameBus side, the player is involved in multiple challenges that all assign different points for trivia games, and where all such challenges could involve a participation )
Please recall that we aim for an extremely loose coupling between GameBus and its mini-games. So, mini-games are not supposed to check leaderboard scores, etc. GameBus does that centrally, and mini-game developers should not bother. Instead, they should check via the `FrameData` that was received from GameBus whether the `negativePointsCheck` array contains a rule item which represents a hint, and then whether that rule is allowed to fire. While this example code is arguably not straight-forward, it does cover a very wide range of scenario's (including cases where at the GameBus side, the player is involved in multiple challenges that all assign different points for trivia games, and where all such challenges could concurrently represent a competition between your friends and another one between your colleagues). When considering the flexibility at the GameBus side, the `canTakeHint` function inside of this mini-game is quite simple.

That's all folks! The other parts of the sources inside this repository are just part of the basic Trivia game implementation, and we only aimed to clarify how the communication with GameBus was realized. Then again, if you have any question or remark then do not hesitate to [contact us](https://twitter.com/gamebusapp).

## Development Instructions


### Test local web via

```
./runLocal.sh
```

This will start an HTTPS local server, provided that you generated the right keys. More information: see [localhost subdirectory](localhost).

This of course requires to have installed:

```
npm install --global http-server
```

### Sanity check

```lighthouse http://127.0.0.1:8080 --view```

### Build for web via:

```
ionic build --release --prod -- --base-href "/opentrivia/" --deploy-url="/opentrivia/"
```
## Deployment

Upload www folder to remote server, include also the .htaccess file and ensure that:

* mod_rewrite is enabled in Apache, and that
* Apache config has AllowOverride All


# License

Copyright 2021, Pieter Van Gorp ([https://www.pietervangorp.com/
(https://www.pietervangorp.com/))

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

1) The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

2) THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

3) Pieter Van Gorp preserves ownership of his own version of the software, and derived works shall not limit the unrestricted use by Pieter Van Gorp.