Adapted from: https://stackoverflow.com/a/61905546/7765510

```
brew install mkcert
mkcert -install
mkcert 0.0.0.0 localhost 127.0.0.1 ::1
mv 0.0.0.0+3-key.pem key.pem
mv 0.0.0.0+3.pem cert.pem
```

[no longer needed AFAIK] In Chrome, open chrome://flags/#allow-insecure-localhost and set "Allow invalid certificates for resources loaded from localhost." to enabled (more info: see https://stackoverflow.com/questions/46349459/chrome-neterr-cert-authority-invalid-error-on-self-signing-certificate-at-loca). 

Relaunch Chrome

Start local https server via 

```
http-server -S -C cert.pem www
```

You can use the same generated certificates for more convenient local development (via ionic serve) as follows:

```
ionic serve --port=8080 --ssl --  --ssl-cert cert.pem --ssl-key key.pem
```
