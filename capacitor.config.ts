import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'opentrivia',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
